<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Api\Data;

interface MemberSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Member list.
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \OnePeople\TeamMember\Api\Data\MemberInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

