<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface DepartmentRepositoryInterface
{

    /**
     * Save Department
     * @param \OnePeople\TeamMember\Api\Data\DepartmentInterface $department
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \OnePeople\TeamMember\Api\Data\DepartmentInterface $department
    );

    /**
     * Retrieve Department
     * @param string $departmentId
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($departmentId);

    /**
     * Retrieve Department matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \OnePeople\TeamMember\Api\Data\DepartmentSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Department
     * @param \OnePeople\TeamMember\Api\Data\DepartmentInterface $department
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \OnePeople\TeamMember\Api\Data\DepartmentInterface $department
    );

    /**
     * Delete Department by ID
     * @param string $departmentId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($departmentId);
}

