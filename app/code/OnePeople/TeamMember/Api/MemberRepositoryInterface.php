<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MemberRepositoryInterface
{

    /**
     * Save Member
     * @param \OnePeople\TeamMember\Api\Data\MemberInterface $member
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \OnePeople\TeamMember\Api\Data\MemberInterface $member
    );

    /**
     * Retrieve Member
     * @param string $memberId
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($memberId);

    /**
     * Retrieve Member matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \OnePeople\TeamMember\Api\Data\MemberSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Member
     * @param \OnePeople\TeamMember\Api\Data\MemberInterface $member
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \OnePeople\TeamMember\Api\Data\MemberInterface $member
    );

    /**
     * Delete Member by ID
     * @param string $memberId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($memberId);
}

