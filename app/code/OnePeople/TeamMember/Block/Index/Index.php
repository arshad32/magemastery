<?php

/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OnePeople\TeamMember\Block\Index;

class Index extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \OnePeople\TeamMember\Model\DepartmentFactory $departmentFactory,
        \OnePeople\TeamMember\Model\MemberFactory $memberFactory,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\ResourceModel\Block $blockResource,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
        $this->departmentFactory = $departmentFactory;
        $this->memberFactory = $memberFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    public function getDepartment()
    {
        return $this->departmentFactory->create()->getCollection();
    }

    public function getMembers()
    {
        return $this->memberFactory->create()->getCollection();
    }

    public function getStaticBlock($identifier)
    {
        try {
            $block = $this->blockFactory->create();
            $block->setStoreId($this->storeManager->getStore()->getId());
            $this->blockResource->load($block, $identifier);
            if (!$block->getId()) {
                return false;
            }
            return $block;
        } catch (\Exception $e) {
            //$this->logger->warning($e->getMessage());
        }
        return false;
    }

    public function getCmsBlockContent($identifier)
    {

        $staticBlock = $this->getStaticBlock($identifier);

        if ($staticBlock && $staticBlock->isActive()) {
            return $staticBlock->getContent();
        }

        return __('Static block content not found');
    }

    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
}
