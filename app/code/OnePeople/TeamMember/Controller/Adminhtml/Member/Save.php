<?php

/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OnePeople\TeamMember\Controller\Adminhtml\Member;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        // print_r($data);
        // exit();
        if ($data) {
            $id = $this->getRequest()->getParam('member_id');

            $model = $this->_objectManager->create(\OnePeople\TeamMember\Model\Member::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Member no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $data = $this->_filterPhotoData($data);
            // print_r($data);
            // exit();
            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Member.'));
                $this->dataPersistor->clear('onepeople_teammember_member');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['member_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Member.'));
            }

            $this->dataPersistor->set('onepeople_teammember_member', $data);
            return $resultRedirect->setPath('*/*/edit', ['member_id' => $this->getRequest()->getParam('member_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function _filterPhotoData(array $rawData)
    {
        //Replace icon with fileuploader field name
        $data = $rawData;
        if (isset($data['photo'][0]['name'])) {
            $data['photo'] = 'teammember/tmp/photomember/' . $data['photo'][0]['name'];
        } else {
            $data['photo'] = null;
        }
        return $data;
    }
}
