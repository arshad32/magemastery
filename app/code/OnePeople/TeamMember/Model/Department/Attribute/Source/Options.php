<?php
namespace OnePeople\TeamMember\Model\Department\Attribute\Source;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array
     */
    public $options = null;

    public function __construct(
      \OnePeople\TeamMember\Model\DepartmentFactory $departmentFactory
    ){
      $this->departmentFactory = $departmentFactory;
    }

    public function toOptionArray() {
        $collections = $this->departmentFactory->create()->getCollection();
        foreach ($collections as $collection) {
          $this->options[] = [
              "label" => $collection->getName(),
              "value" => $collection->getId()
          ];
        }
        return $this->options;
    }
}