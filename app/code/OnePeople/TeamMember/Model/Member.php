<?php

/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OnePeople\TeamMember\Model;

use Magento\Framework\Api\DataObjectHelper;
use OnePeople\TeamMember\Api\Data\MemberInterface;
use OnePeople\TeamMember\Api\Data\MemberInterfaceFactory;

class Member extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'onepeople_teammember_member';
    protected $memberDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param MemberInterfaceFactory $memberDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \OnePeople\TeamMember\Model\ResourceModel\Member $resource
     * @param \OnePeople\TeamMember\Model\ResourceModel\Member\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        MemberInterfaceFactory $memberDataFactory,
        DataObjectHelper $dataObjectHelper,
        \OnePeople\TeamMember\Model\ResourceModel\Member $resource,
        \OnePeople\TeamMember\Model\ResourceModel\Member\Collection $resourceCollection,
        array $data = [],
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->memberDataFactory = $memberDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->authSession = $authSession;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve member model with member data
     * @return MemberInterface
     */
    public function getDataModel()
    {
        $memberData = $this->getData();

        $memberDataObject = $this->memberDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $memberDataObject,
            $memberData,
            MemberInterface::class
        );

        return $memberDataObject;
    }

    public function beforeSave()
    {
        $createdAt = $this->getCreatedAt();
        $currentTime = date("Y-m-d H:i:s");
        if (empty($createdAt)) {
            $createdAt = $currentTime;
            $this->setCreatedAt($createdAt);
            $this->setUpdatedAt($currentTime);
        } else {
            $this->setUpdatedAt($currentTime);
        }

        $createdBy = $this->getCreatedBy();
        $currentAdminId = $this->authSession->getUser()->getId();
        if (empty($createdBy)) {
            $createdBy = $currentAdminId;
            $this->setCreatedBy($currentAdminId);
            $this->setUpdatedBy($currentAdminId);
        } else {
            $this->setUpdatedBy($currentAdminId);
        }
    }
}
