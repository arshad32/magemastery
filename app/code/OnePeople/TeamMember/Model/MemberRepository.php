<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use OnePeople\TeamMember\Api\Data\MemberInterfaceFactory;
use OnePeople\TeamMember\Api\Data\MemberSearchResultsInterfaceFactory;
use OnePeople\TeamMember\Api\MemberRepositoryInterface;
use OnePeople\TeamMember\Model\ResourceModel\Member as ResourceMember;
use OnePeople\TeamMember\Model\ResourceModel\Member\CollectionFactory as MemberCollectionFactory;

class MemberRepository implements MemberRepositoryInterface
{

    protected $memberCollectionFactory;

    protected $extensibleDataObjectConverter;
    protected $memberFactory;

    protected $resource;

    protected $dataObjectHelper;

    protected $dataMemberFactory;

    private $storeManager;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceMember $resource
     * @param MemberFactory $memberFactory
     * @param MemberInterfaceFactory $dataMemberFactory
     * @param MemberCollectionFactory $memberCollectionFactory
     * @param MemberSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMember $resource,
        MemberFactory $memberFactory,
        MemberInterfaceFactory $dataMemberFactory,
        MemberCollectionFactory $memberCollectionFactory,
        MemberSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->memberFactory = $memberFactory;
        $this->memberCollectionFactory = $memberCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMemberFactory = $dataMemberFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \OnePeople\TeamMember\Api\Data\MemberInterface $member
    ) {
        /* if (empty($member->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $member->setStoreId($storeId);
        } */
        
        $memberData = $this->extensibleDataObjectConverter->toNestedArray(
            $member,
            [],
            \OnePeople\TeamMember\Api\Data\MemberInterface::class
        );
        
        $memberModel = $this->memberFactory->create()->setData($memberData);
        
        try {
            $this->resource->save($memberModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the member: %1',
                $exception->getMessage()
            ));
        }
        return $memberModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($memberId)
    {
        $member = $this->memberFactory->create();
        $this->resource->load($member, $memberId);
        if (!$member->getId()) {
            throw new NoSuchEntityException(__('Member with id "%1" does not exist.', $memberId));
        }
        return $member->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->memberCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \OnePeople\TeamMember\Api\Data\MemberInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \OnePeople\TeamMember\Api\Data\MemberInterface $member
    ) {
        try {
            $memberModel = $this->memberFactory->create();
            $this->resource->load($memberModel, $member->getMemberId());
            $this->resource->delete($memberModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Member: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($memberId)
    {
        return $this->delete($this->get($memberId));
    }
}

